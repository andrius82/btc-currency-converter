import React from 'react';
import '../scss/Message.scss';

interface Props {
   message: string;
}

const Message = (props: Props) => (
	props.message !== "" ?
		<div className="message">
         {props.message}
		</div>
	:null
);


export default Message;