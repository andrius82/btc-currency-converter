import React from 'react';
import '../scss/Loader.scss';


interface Props {
   loading: boolean;
}

const Loader = (props: Props) => (
   <>
      {props.loading && <div className="loader"></div>}
   </>
);


export default Loader;