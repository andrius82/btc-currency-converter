import React, { useState, useEffect } from 'react';
import '../scss/BtcCurrencyConverter.scss';

import useInterval from "../hooks/useInterval.js";
import _ from 'lodash';
import axios from 'axios';
import Loader from './Loader';
import Message from './Message';


function BtcCurrencyConverter() {

	const [btcValue, setBtcValue] = useState(1);
	const [data, setData] = useState(null);
	const [currencies, setCurrencies] = useState(null);
	const [loading, setLoading] = useState(false);
	const [message, setMessage] = useState("");


	useEffect(() => {
		setLoading(true);
		fetchData();
	}, []);


	useInterval(() => {
		setLoading(true);
		fetchData();
	}, 60000);

	
	function fetchData() {
		axios.get('https://api.coindesk.com/v1/bpi/currentprice.json')
			.then(res => {
				if(res.data.length !== 0){
					if (data === null) {
						setData(res.data.bpi);	
						// add property - visible
						const newdata = _.cloneDeep(res.data.bpi);
						Object.values(newdata).forEach((currency, index) => {
							if (index === 0) {
								currency.visible = true;
							}else {
								currency.visible = false;
							}

							if (index === Object.keys(newdata).length - 1) {
								setCurrencies(newdata);
							}
						});
					}	

					if (data !== null) {
						setData(res.data.bpi);
						setCurrencies(res.data.bpi);
						recalculateCurencies(btcValue);
					}		

					_.delay(() => {
						setLoading(false);
					}, 800);	
				}
			}).catch(error => {
				let msgs = "Error - cannot get data from server";
				setMessage(msgs);
			});
	}


	function recalculateCurencies(btcVal) {
		const newcurrencies = _.cloneDeep(currencies);
		Object.keys(newcurrencies).map(item => newcurrencies[item].rate_float = data[item].rate_float * btcVal);
		setCurrencies(newcurrencies);
	}


	function recalculateBTC(currencyCode, value) {
		const ratio = value / data[currencyCode].rate_float;
		setBtcValue(ratio);
		recalculateCurencies(ratio);
	}


	function formatDigits(digits) {
		return digits.toLocaleString('en-EN', { maximumFractionDigits: 2 });
	}


    function handleInputChange(event) {
		// preserve cursor
		const caret = event.target.selectionStart;
		const el = event.target;
		window.requestAnimationFrame(() => {
			el.selectionStart = caret;
			el.selectionEnd = caret;
		});	

		//clean commas
		const unformatted = event.target.value.replace(/,/g, '');

		if (unformatted.match(/^-?\d*\.?\d*$/)) {
			if (event.target.name === 'BTC') {
				setBtcValue(Number(unformatted));
				recalculateCurencies(Number(unformatted));
			}

			if (event.target.name !== 'BTC') {
				setCurrencies({
					...currencies,
					[event.target.name]: { ...currencies[event.target.name], rate_float: Number(unformatted) }
				});	
				recalculateBTC(event.target.name, Number(unformatted));
			}
		}
    }


	function handleInputRemove(currencyCode) {
		const newcurrencies = _.cloneDeep(currencies);
		newcurrencies[currencyCode].visible = false;
		setCurrencies(newcurrencies);
	}


	function handleSelectChange(event) {
		const newcurrencies = _.cloneDeep(currencies);
		newcurrencies[event.target.value].visible = true;
		setCurrencies(newcurrencies);
	}

	
	return(
		<div className="wrapper">
			<Message message={message} />
			{currencies &&
			<form noValidate>
				<div className="form-item">
					<input 
						name="BTC" 
						type="tel" 
						onChange={(ev) => handleInputChange(ev)} 
						placeholder="..."
						value={formatDigits(btcValue)}
						inputMode="numeric"
						pattern="\d*"
					/>
					<label htmlFor="BTC">BTC</label>
				</div>
				{Object.values(currencies).map(currency => (
					<div className={`form-item ${currency.visible || 'hidden'}`}  key={currency.code}>
						<div onClick={() => handleInputRemove(currency.code)}  className="form-item__remove"></div>
						<input 
							name={currency.code} 
							type="tel" 
							onChange={(ev) => handleInputChange(ev)} 
							placeholder="..."
							value={formatDigits(currency.rate_float)}	
							inputMode="numeric"
							pattern="\d*"						
						/>
						<label htmlFor="eur">{currency.code}</label>
					</div>
				))}
				{Object.values(currencies).filter(currency => currency.visible === false).length !== 0 &&
					<div className="form-item">
						<label htmlFor="currencies">Select currency</label>
						<select 
							name="currencies" 
							id="currencies" 
							onChange={(ev) => handleSelectChange(ev)} 
						>
							<option key={0} value={0}>Add currency</option>
							{ Object.values(currencies).map(currency => currency.visible || <option key={currency.code} value={currency.code}>{currency.code}</option>) }
						</select>
					</div>
				}
			</form>
			}
			<Loader loading={loading} />
		</div>
	);
}

export default BtcCurrencyConverter;