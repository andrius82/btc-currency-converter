import React from 'react';
import BtcCurrencyConverter from './BtcCurrencyConverter';

function App() {
  return (
    <BtcCurrencyConverter />
  );
}

export default App;
